import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import federation from "@originjs/vite-plugin-federation";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 3010,
  },
  preview: {
    port: 3010,
  },
  build: {
    modulePreload: false,
    target: "esnext",
    minify: false,
    cssCodeSplit: false,
  },
  plugins: [
    react(),
    federation({
      filename: "root-app-mfe-entry.js",
      name: "root-app-mfe",
      shared: ["react", "react-dom"],
      // exposes: {
      //   "./App": "./src/App.tsx",
      // },
      remotes: {
        "child-app-1-mfe": "http://localhost:3011/assets/child-app-1-mfe-entry.js",
        "child-app-2-mfe": "http://localhost:3012/assets/child-app-2-mfe-entry.js",
        "child-app-3-mfe": "http://localhost:3013/assets/child-app-3-mfe-entry.js",
      },
    }),
  ],
})
