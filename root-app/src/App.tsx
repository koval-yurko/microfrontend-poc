import React, { Suspense } from "react";
const ChildApp1 = React.lazy(() => import("child-app-1-mfe/App"));
const ChildApp2 = React.lazy(() => import("child-app-2-mfe/App"));
const ChildApp3 = React.lazy(() => import("child-app-3-mfe/App"));

function App() {
  return (
      <div className="flex min-h-[100vh] justify-center items-center flex-col">
          <div>Root App</div>
          <div>
              <Suspense fallback={<div>Loading...</div>}>
                  <ChildApp1/>
              </Suspense>
          </div>
          <div>
              <Suspense fallback={<div>Loading...</div>}>
                  <ChildApp2/>
              </Suspense>
          </div>
          <div>
              <Suspense fallback={<div>Loading...</div>}>
                  <ChildApp3/>
              </Suspense>
          </div>
      </div>
  )
}

export default App
