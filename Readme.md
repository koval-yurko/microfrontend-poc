# Microfrontend - Vite + React + Tailwind

## Getting Started

Build each sub-app (child-app-1, child-app-2, child-app-3) with:
```
cd ./child-app-1
npm i
npm run build
npm run preview
```

Build and run the root-app with:
```
cd ./root-app
npm i
npm run build
npm run preview
```

Open browser on http://localhost:3010
