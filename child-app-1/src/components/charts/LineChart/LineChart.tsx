import React from "react";
import { AxisOptions, Chart, UserSerie } from "react-charts";

type LineChartProps = {
    data: UserSerie<any>[];
    height: number;
}

export function LineChart({ data, height = 300 }: LineChartProps) {

    const primaryAxis = React.useMemo<
        AxisOptions<typeof data[number]["data"][number]>
    >(
        () => ({
            getValue: (datum) => datum.primary as unknown as Date,
        }),
        []
    );

    const secondaryAxes = React.useMemo<
        AxisOptions<typeof data[number]["data"][number]>[]
    >(
        () => [
            {
                getValue: (datum) => datum.secondary,
            },
        ],
        []
    );

    return (
        <div style={{ height }}>
            <Chart
                options={{
                    data,
                    primaryAxis,
                    secondaryAxes,
                }}
            />
        </div>
    );
}
