import { LineChart } from './components/charts/LineChart'
import { useChartConfig } from "./useDemoConfig.tsx";

function App() {
    const { data: lineChartData, randomizeData: randomizeLineChartData } = useChartConfig({
        series: 1,
        dataType: "time",
    });

    return (
        <div className="max-w-6xl mx-auto px-4 py-10">
            <div className="grid grid-cols-3 gap-4">
                <div className="rounded-lg shadow-md bg-white p-4">
                    <div className="text-3xl">77h</div>
                    <div>Some small text</div>
                </div>
                <div className="rounded-lg shadow-md bg-white p-4">
                    <div className="text-3xl">77h</div>
                    <div>Some small text</div>
                </div>
                <div className="rounded-lg shadow-md bg-white p-4">
                    <div className="text-3xl">77h</div>
                    <div>Some small text</div>
                </div>

                <div className="col-span-2 rounded-lg shadow-md bg-white p-4">
                    <div className="min-h-full flex flex-col justify-between">
                        <div className="mb-4">
                            <div className="text-2xl mb-4">Line chart</div>
                            <LineChart height={300} data={lineChartData} />
                        </div>
                        <div>
                            <button
                                className="w-full text-blue-500 border border-blue-500 rounded-md text-sm px-2 py-1"
                                onClick={randomizeLineChartData}
                            >
                                Randomize data
                            </button>
                        </div>
                    </div>
                </div>
                <div className="rounded-lg shadow-md bg-white p-4">
                    <div className="min-h-full flex flex-col justify-between">
                        <div className="text-3xl">77h</div>
                        <div>
                            <button
                                className="w-full text-blue-500 border border-blue-500 rounded-md text-sm px-2 py-1">Button
                                text
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default App
