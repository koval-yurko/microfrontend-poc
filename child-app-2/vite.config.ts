import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import federation from "@originjs/vite-plugin-federation";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 3012,
    host: '0.0.0.0'
  },
  preview: {
    port: 3012,
    host: '0.0.0.0'
  },
  build: {
    modulePreload: false,
    target: "esnext",
    minify: false,
    cssCodeSplit: false,
  },
  plugins: [
    react(),
    federation({
      filename: "child-app-2-mfe-entry.js",
      name: "child-app-2-mfe",
      shared: ["react", "react-dom"],
      exposes: {
        "./App": "./src/App.tsx",
      },
    }),
  ],
})
